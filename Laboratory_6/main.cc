/* Лабораторная работа 11-12 */

#define NDEBUG

#include <iostream>
// #include <assert.h>
// #include <iomanip>
#include "../pz17p1.hpp"

struct Node
{
    int info;
    int h;
    Node *left = NULL;
    Node *right = NULL;
};

static bool is_find(false);

int height(Node *p);
int bfactor(Node *p);
void fixheight(Node *p);
Node *rotateright(Node *p);   // правий поворот навколо p
Node *rotateleft(Node *q);    // левый поворот вокруг q
Node *balance(Node *p);       // балансировка узла p
Node *insert(Node *p, int k); // вставка ключа k в дерево с корнем p
void printh(Node *node, int level = 0);
void find(Node *node, int key, int level = 0);
void remove(Node *node);

int main(int argc, const char **argv)
{
    Node *node;

    pz::Menu menu(new std::string[6]{"Створення збалансованого бінарного дерева",
                                     "Перегляд вмісту бінарного дерева",
                                     "Пошук ключового елементу",
                                     "Видалення дерева",
                                     "Вихід"});

    do
    {
        switch (menu.run())
        {
        case 1: // Створення збалансованого бінарного дерева
        {
            system("clear");
            int value(0);
            do
            {
                std::cout << "Введите елемент для вставки /> ";
                pz::input_check(value, "Error: Введите елемент для вставки /> ");
                node = insert(node, value);

                std::cout << "Продолжить (Y/n)?: ";
                if (strrchr("yY0\n", (char)getchar()) == nullptr)
                    break;

            } while (true);

            while (getchar() != '\n')
                continue;

            break;
        }
        case 2: // Перегляд вмісту бінарного дерева
        {
            system("clear");

            if (node == NULL)
            {
                std::cout << "\033[2;32mДерево пустое!\033[0m" << std::endl;
                getchar();
                break;
            }

            printh(node);
            getchar();
            break;
        }
        case 3: // Пошук ключового елементу
        {
            system("clear");
            if (node == NULL)
            {
                std::cout << "\033[2;32mДерево пустое!\033[0m" << std::endl;
                getchar();
                break;
            }

            int value(0);
            std::cout << "Введите искомый елемент /> ";
            pz::input_check(value, "Error: Введите искомый елемент /> ");
            find(node, value);

            if (is_find == false)
                std::cout << "\033[2;47m\033[2;31m Елемент не найден! \033[0m" << std::endl;

            is_find = false;

            getchar();
            break;
        }
        case 4: // Видалення дерева
        {
            system("clear");
            remove(node);
            node = NULL;

            std::cout << "\033[2;32mДерево удаленно!\033[0m" << std::endl;
            getchar();

            break;
        }
        case 5: // Вихід
            return EXIT_SUCCESS;
        default:
            break;
        }

    } while (true);
}

int height(Node *p)
{
    // if (p == NULL)
    // {
    //     return 0;
    // }

    return (p == NULL ? 0 : p->h);
}

int bfactor(Node *p)
{
    return height(p->right) - height(p->left);
}

void fixheight(Node *p)
{
    if (p != NULL || p->left != NULL || p->right != NULL)
    {
        int h1 = height(p->left);
        int h2 = height(p->right);

        p->h = (h1 > h2 ? h1 : h2) + 1;

        // if (h1 > h2)
        //     p->h = h1 + 1;
        // else
        //     p->h = h2 + 1;
    }
}

Node *rotateright(Node *p) // правий поворот навколо p
{
    Node *q = p->left;
    p->left = q->right;
    q->right = p;
    fixheight(p);
    fixheight(q);
    return q;
}

Node *rotateleft(Node *q) // левый поворот вокруг q
{
    Node *p = q->right;

    q->right = p->left;
    p->left = q;

    fixheight(q);
    fixheight(p);

    return p;
}

Node *balance(Node *p) // балансировка узла p
{
    fixheight(p);
    if (bfactor(p) == 2)
    {
        if (bfactor(p->right) < 0)
            p->right = rotateright(p->right);
        return rotateleft(p);
    }

    if (bfactor(p) == -2)
    {
        if (bfactor(p->left) > 0)
            p->left = rotateleft(p->left);
        return rotateright(p);
    }

    return p; // балансировка не нужна
}

Node *insert(Node *p, int k) // вставка ключа k в дерево с корнем p
{
    if (p == NULL)
    {
        p = new Node;
        p->info = k;
        p->h = 0;
        p->left = NULL;
        p->right = NULL;
    }
    else if (k < p->info)
        p->left = insert(p->left, k);
    else
        p->right = insert(p->right, k);

    return balance(p);
}

void find(Node *node, int key, int level)
{
    // if (node == NULL)
    // {
    //     std::cout << std::string(level, '\t') << std::string(3, '\b') << "\033[2;32mnull\033[0m" << std::endl;
    //     return;
    // }
    // else if (key == node->info)
    // {
    //     std::cout << std::string(level, '\t') << std::string(3, '\b') << "\033[2;47m\033[2;31m " << node->info << " \033[0m" << std::endl;
    //     return;
    // }
    // else if (key > node->info)
    // {
    //     find(node->right, key, level + 1);
    //     std::cout << std::string(level, '\t') << std::string(3, '\b') << "\033[2;33m" << node->info << "\033[0m" << std::endl;
    // }

    // else if (key < node->info)
    // {
    //     find(node->left, key, level + 1);
    //     std::cout << std::string(level, '\t') << std::string(3, '\b') << "\033[2;33m" << node->info << "\033[0m" << std::endl;
    // }

    if (node == NULL)
    {
        std::cout << std::string(level, '\t') << std::string(3, '\b') << "\033[2;32mnull\033[0m" << std::endl;
        return;
    }

    find(node->right, key, level + 1);

    if (node->info == key)
    {
        is_find = true;
    }

    std::cout << std::string(level, '\t') << std::string(3, '\b') << (node->info == key ? "\033[2;47m\033[2;31m " : "\033[2;33m")
              << node->info << " \033[0m" << std::endl;
    find(node->left, key, level + 1);
}

void printh(Node *node, int level)
{
    if (node == NULL)
    {
        std::cout << std::string(level, '\t') << std::string(3, '\b') << "\033[2;32mnull\033[0m" << std::endl;
        return;
    }

    printh(node->right, level + 1);
    std::cout << std::string(level, '\t') << std::string(3, '\b') << "\033[2;33m" << node->info << "\033[0m" << std::endl;
    printh(node->left, level + 1);
}

void remove(Node *node)
{
    if (node != NULL)
    {
        remove(node->right);
        remove(node->left);
        delete node;
        node = NULL;
    }
}
