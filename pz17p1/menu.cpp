#include "menu.hpp"

pz::Menu::Menu(std::string *menu, bool clear)
    : clear(clear)
{
    while (!menu[size].empty())
        this->menu.push_back(menu[size++]);
}

pz::Menu::~Menu()
{
    
}

size_t pz::Menu::run()
{
    int answer = 0;

    do
    {
        if (clear == true)
            system("clear");

        std::cout << this->caption;

        for (size_t i(0); i < this->size; i++)
            std::cout << std::endl
                      << ((this->last == i) ? this->ptrl : std::string(ptrl.size(), ' ')) << this->menu.at(i)
                      << ((this->last == i) ? this->ptrr : "");

        std::cout << "\n\033[0;35mУправление\033[1;33m(↑↓)\033[0;35m\n"
                  << "Выбор\033[1;33m<Enter>\033[0m";

        answer = pz::_getch();

        if (answer == KEY_DOWN && this->last < this->size)
            this->last++;
        if (answer == KEY_DOWN && this->last == this->size)
            this->last = 0;
        else if (answer == KEY_UP && this->last > 0)
            this->last--;
        else if (answer == KEY_UP && this->last == 0)
            this->last = this->size - 1;

    } while (answer != ENTER);

    return this->last + 1;
}

size_t pz::Menu::get_last_selected()
{
    return this->last + 1;
}

void pz::Menu::set_clear(bool clear)
{
    this->clear = clear;
}

void pz::Menu::show_cursor(bool flag)
{
    system("tput reset ");

    system((flag) ? "tput cnorm" : "tput civis");
}