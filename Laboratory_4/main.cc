/* Лабораторная работа 9-10 */

#define NDEBUG

#include <iostream>
#include <assert.h>
#include <iomanip>
#include "../pz17p1.hpp"

typedef int T;

void quickSort(int arr[], int left, int right);

int main(int argc, char *argv[])
{
    int row(6), /* строки матрицы */
        col(6); /* столбцы матрицы */
    T **matrix = NULL;
    pz::Menu menu(new std::string[6]{"Створення матриці",
                                     "Виведення матриці",
                                     "Відсортувати матрицю за стовпцями",
                                     "Видалення матриці",
                                     "Вихід"});

    do
    {
        switch (menu.run())
        {
        case 1: // Створення матриці
        {
            do
            {
                pz::input_check(row,
                                "\nВведіть число рядків матриці -> ",
                                "Error: Введіть число рядків матриці -> ");

            } while (row <= 0);

            do
            {
                pz::input_check(col,
                                "Введіть число стовпців матриці -> ",
                                "Error: Введіть число стовпців матриці -> ");

            } while (col <= 0);

            matrix = new T *[row];
            for (int i(0); i < row; i++)
                matrix[i] = new T[col];

            // for (size_t i(0); i < row; i++)
            //     for (size_t j(0); j < col; j++)
            //         matrix[i][j] = rand() % 10;

            std::cout << "Fill matrix:" << std::endl;
            for (int i(0); i < row; i++)
                for (int j(0); j < col; j++)
                {
                    std::cout << "matrix[" << i << "][" << j << "] -> ";
                    pz::input_check(matrix[i][j]);
                }

            break;
        }
        case 2: // Виведення матриці
        {
            std::cout << std::endl;
            for (int i(0); i < row; i++)
                for (int j(0); j < col; j++)
                    std::cout << std::setw(6) << matrix[i][j] << (j == col - 1 ? '\n' : ' ');

            getchar();
            break;
        }
        case 3: // Відсортувати матрицю за стовпцями
        {
            int first_row(0), second_row(0);

            do
            {
                pz::input_check(first_row,
                                "\nВведите первый сортируемый столбец -> ",
                                "Error: Введите первый сортируемый столбец -> ");
            } while (first_row < 1 || first_row > row);

            do
            {
                pz::input_check(second_row,
                                "Введите второй сортируемый столбец -> ",
                                "Error: Введите второй сортируемый столбец -> ");
            } while (second_row < 1 || second_row > row);

            for (int i(0); i < col; i++)
            {
                if (i == first_row - 1 || i == second_row - 1)
                {
                    T *tmp = new T[col];

                    for (int j(0); j < row; j++)
                        tmp[j] = matrix[j][i];

                    // sort_shell(tmp, row);
                    quickSort(tmp, 0, row);

                    for (int j(0); j < row; j++)
                        matrix[j][i] = tmp[j];

                    delete tmp;
                }
            }

            std::cout << std::endl
                      << "Матриця відсортована по стовпцям" << std::endl;
            getchar();
            break;
        }
        case 4: // Видалення матриці
        {
            if (matrix != NULL)
            {
                for (int i(0); i < row; i++)
                    delete[] matrix[i];

                delete[] matrix;
            }

            std::cout << std::endl
                      << "Матриця!";
            getchar();

            break;
        }
        case 5: // Вихід
        {
            if (matrix != NULL)
            {
                for (int i(0); i < row; i++)
                    delete[] matrix[i];

                delete[] matrix;
            }

            return EXIT_SUCCESS;
        }
        default:
            break;
        }

    } while (true);
}

void quickSort(int arr[], int left, int right)
{
    int i(left),
        j(right),
        pivot(arr[(left + right) / 2]);

    while (i <= j)
    {
        while (arr[i] < pivot) i++;
        while (arr[j] > pivot) j--;

        if (i <= j) std::swap(arr[i++], arr[j--]);
    }

    if (left < j)
        quickSort(arr, left, j);

    if (i < right)
        quickSort(arr, i, right);
}