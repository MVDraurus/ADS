#ifndef TREE_HPP
#define TREE_HPP

#include <iostream>

#define SPACE 2

/**
 * @brief Класс реализации бинарного дерева.
 * 
 * @tparam T Стандартный тип даных
 */
template <typename T>
class Tree
{
  private:
    struct TreeNode
    {
        TreeNode(){};
        TreeNode(T data) : data(data){};

        T data;
        TreeNode *left = NULL,
            *right = NULL;
    };

    TreeNode *create_tree(char k)
    {
        TreeNode *work;

        if (k != 'y')
            return NULL;

        work = new TreeNode();

        std::cout << "Enter value: ";
        std::cin >> work->data;

        std::cout << "Continue left?: ";
        std::cin >> k;
        work->left = create_tree(k);

        std::cout << "Continue right?: ";
        std::cin >> k;
        work->right = create_tree(k);

        return work;
    }

    void printh(TreeNode *node, int level = 0)
    {
        if (node == NULL)
        {
            std::cout << std::string(level, '\t') << "NULL" << std::endl;
            return;
        }

        printh(node->left, level + 1);
        std::cout << std::string(level, '\t') << node->data << std::endl;
        printh(node->right, level + 1);
    }

    void printv(TreeNode *treeNode, int size, int step, int polov)
    {
        static int count(0);
        static int max(0);

        if (count > max)
            max = count;

        if (treeNode == NULL)
            return;

        if (step == 2 || step == -6 || step == -12 || step == -10 || step == -14)
            std::cout << std::string(2, '\n');

        std::cout << std::string((size * 2) + step, ' ') << treeNode->data
                  << std::string(SPACE, '\n');

        count += SPACE;
        printv(treeNode->right, size, step + polov, polov / SPACE);

        std::cout << "\033[" << count << ";" << 0 << "H" << std::endl;
        count -= SPACE;

        printv(treeNode->left, size, step - polov, polov / SPACE);

        std::cout << "\033[" << max << ";" << 0 << "H" << std::endl;
    }

    void print_min_max(TreeNode *treeNode)
    {
        if (treeNode != NULL)
        {
            print_min_max(treeNode->left);
            print_min_max(treeNode->right);

            if (treeNode->data > max)
                max = treeNode->data;

            if (treeNode->data < min)
                min = treeNode->data;
        }
    }

    int depth_tree(TreeNode *treeNode, int c)
    {
        static int max(0);

        if (c > max)
            max = c;

        if (treeNode->left != NULL)
            depth_tree(treeNode->left, c + 1);

        if (treeNode->right != NULL)
            depth_tree(treeNode->right, c + 1);

        return max + 1;
    }

    TreeNode *treeNode = NULL;
    int max = 0, min = 0;

  public:
    Tree(){};
    ~Tree(){};

    void create()
    {
        treeNode = create_tree('y');
    }

    void printv()
    {
        printv(treeNode, depth() * SPACE, 0, depth() * SPACE);
    }

    void printh()
    {
        printh(treeNode, 0);
    }

    int depth()
    {
        return depth_tree(this->treeNode, 0);
    }

    void fill()
    {
        treeNode = new TreeNode(0);

        treeNode->left = new TreeNode(-1);
        treeNode->left->left = new TreeNode(3.0f);

        treeNode->left->right = new TreeNode(-3);

        treeNode->right = new TreeNode(1);
        treeNode->right->left = new TreeNode(-2.0f);
        treeNode->right->left->left = new TreeNode(8);
        treeNode->right->left->right = new TreeNode(4);

        treeNode->right->right = new TreeNode(-7.0f);
        treeNode->right->right->left = new TreeNode(3);
        treeNode->right->right->right = new TreeNode(-2.0f);
    }

    void print_min_max()
    {
        print_min_max(treeNode);

        std::cout << std::endl
                  << "min->" << min << std::endl
                  << "max->" << max << std::endl;
    }
};

#endif // TREE_HPP