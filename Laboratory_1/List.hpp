#ifndef LIST_HPP
#define LIST_HPP

#include <iostream>

/**
 * @brief Класс реализует список удобный для управления,
 * аналогичен std::vector
 * 
 * @tparam C Стандатный тип даных
 */
template <class C>
class List
{
  private:
	/**
	 * @brief Структура ячейки списка.
	 * 
	 */
	struct Node
	{
		/**
		 * @brief Construct a new Node object
		 * 
		 * @param value Содержимое ячейки
		 */
		Node(C value)
		{
			this->value = value;
		}

		Node()
		{
		}

		C value;		   // значение одной ячейки списка
		Node *next = NULL; // указатель на следующюю ячейку
	};

	Node *node = NULL; // the beginning of the list
  public:
	~List();

	/**
	 * @brief Функция вставляет объект в указаную позицию.
	 * В случае отрицательного индекса вставка не произойдет,
	 * в случае черезмерного индекса, вставка произойдет в конец списка.
	 * 
	 * @param index Позиция вставка [0-] 
	 * @tparam value Обект для вставка 
	 */
	void insert(size_t index, C value);

	/**
	 * @brief Вставка объекта в конец списка.
	 * 
	 * @tparam value Обект для вставка
	 */
	void push_end(C value);

	/**
	 * @brief Вставка объекта в начало списка.
	 * 
	 * @tparam value Обект для вставка
	 */
	void push_start(C value);

	/**
	 * @brief Возращает объект списка.
	 * 
	 * @param index Позиция объекта в списке
	 * @return C Объект
	 */
	C get(size_t index);

	/**
	 * @brief Удаляет объект списка.
	 * 
	 * @param index Позиция объекта в списке
	 * @return C Объек удалён ?
	 */
	bool remove(size_t index);

	/**
	 * @brief Возращает последний объект списка.
	 * 
	 * @return C Объект
	 */
	C get_last();

	/**
	 * @brief Вернет длину списка
	 * 
	 * @return size_t Длина списка
	 */
	size_t size();

	/**
	 * @brief Функция удаляет все елементы со списка
	 */
	void clear();

	/**
	 * @brief Функция изменяет значение ячейки по указаному
	 * индексу.
	 * 
	 * @param index Индекс елемента массива [0-] 
	 * @param value Новое значение
	 * @return true Замена прошла успешно
	 * @return false Замена не получилась
	 */
	bool set(size_t index, C value);
};

#endif // LIST_HPP
