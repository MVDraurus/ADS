#include "List.hpp"

template <class C>
List<C>::~List()
{
    this->clear();
}

template <class C>
void List<C>::push_end(C value)
{
    this->insert(this->size(), value);
}

template <class C>
void List<C>::push_start(C value)
{
    // REVIEW: Как можно обойтись без етого кода и использовать insert()
    Node *tmp = new Node(value);
    tmp->next = this->node;
    node = tmp;
}

template <class C>
void List<C>::insert(size_t index, C value)
{
    if (index < 0)
        return;
    else if (node == NULL)
    {
        node = new Node(value);
        return;
    }
    else if (index == 0)
        this->push_start(value);

    Node *new_node = new Node(value);
    Node *iter = this->node;

    for (size_t i(0); i <= index && iter->next != NULL; i++)
        iter = iter->next;

    new_node->next = iter->next;
    iter->next = new_node;
}

template <class C>
size_t List<C>::size()
{
    if (this->node == NULL)
        return 0;

    size_t index(0);
    Node *iter = this->node;

    while (iter != NULL)
    {
        iter = iter->next;
        index++;
    }

    return index;
}

template <class C>
C List<C>::get(size_t index)
{
    if (this->node == NULL)
        return *new C();

    Node *iter = this->node;
    int x(0);

    while (iter->next != NULL && x < index)
    {
        iter = iter->next;
        x++;
    }

    return iter->value;
}

template <class C>
C List<C>::get_last()
{
    if (this->node == NULL)
        return *new C();

    Node *node = this->node;

    for (size_t i(0); i <= this->size() - 1 && node->next != NULL; i++)
        node = node->next;

    return node->value;
}

template <class C>
bool List<C>::remove(size_t index)
{
    Node *iter = node,
         *tmp = NULL;

    if (index == 0) // if the item in the head of the list
    {
        tmp = node;
        node = node->next;
    }
    else
    {
        for (size_t i(0); i < index - 1 && iter->next != NULL; i++)
            iter = iter->next;

        tmp = iter->next;
        iter->next = tmp->next;
    }

    delete tmp;
    return true;
}

template <class C>
void List<C>::clear()
{
    Node *tmp;

    while (this->node != NULL)
    {
        tmp = node;
        node = node->next;
        delete tmp;
    }
}

template <class C>
bool List<C>::set(size_t index, C value)
{
    if (index < 0 || index > this->size())
        return false;

    Node *iter = node;

    for (size_t i(0); i < index && iter->next != NULL; i++)
        iter = iter->next;

    iter->value = value;

    return true;
}

template class List<int>;
template class List<float>;