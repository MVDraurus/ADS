#ifndef STACK_HPP
#define STACK_HPP

#include <cassert> // для assert
#include <iostream>
#include <iomanip> // для setw

template <typename T>
class Stack
{
  private:
    T *stackPtr;    // указатель на стек
    const int size; // максимальное количество элементов в стеке
    int top;        // номер текущего элемента стека
  public:
    Stack(int = 10);         // по умолчанию размер стека равен 10 элементам
    Stack(const Stack<T> &); // конструктор копирования
    ~Stack();                // деструктор

    inline void push(const T &);     // поместить элемент в вершину стека
    inline T pop();                  // удалить элемент из вершины стека и вернуть его
    inline void printStack();        // вывод стека на экран
    inline const T &Peek(int) const; // n-й элемент от вершины стека
    inline int getStackSize() const; // получить размер стека
    inline T *getPtr() const;        // получить указатель на стек
    inline int getTop() const;       // получить номер текущего элемента в стеке
};

/**
 * @brief Construct a new Stack< T>:: Stack object
 * 
 * @tparam T Тип даных стека
 * @param maxSize Зарезервированая длина стека
 */
template <typename T>
Stack<T>::Stack(int maxSize) : size(maxSize)
{
    stackPtr = new T[size];
    top = 0;          
}

/**
 * @brief Construct a new Stack< T>:: Stack object
 * Коструктор копирования
 * 
 * @tparam T Тип даных стека
 * @param otherStack Копируемый объект
 */
template <typename T>
Stack<T>::Stack(const Stack<T> &otherStack) : size(otherStack.getStackSize())
{
    stackPtr = new T[size];
    top = otherStack.getTop();

    for (int ix = 0; ix < top; ix++)
        stackPtr[ix] = otherStack.getPtr()[ix];
}

template <typename T>
Stack<T>::~Stack()
{
    delete[] stackPtr;
}

/**
 * @brief Метод добавления элемента в стек
 * 
 * @tparam T Тип елементов стека
 * @param value Новый елемент стека
 */
template <typename T>
inline void Stack<T>::push(const T &value)
{
    assert(top < size);

    stackPtr[top++] = value;
}

/**
 * @brief Метод удаления элемента из стека
 * 
 * @tparam T Тип елементов стека
 * @return T Тип елемента стека
 */
template <typename T>
inline T Stack<T>::pop()
{
    assert(top > 0); 
    
    stackPtr[--top]; 
}

/**
 * @brief Метод возвращает n-й элемент от вершины стека
 * 
 * @tparam T Тип елементов стека
 * @param nom Номер елемента в стеке
 * @return const T& Елемент расположеный по даному индексу
 */
template <class T>
inline const T &Stack<T>::Peek(int nom) const
{
    assert(nom <= top);

    return stackPtr[top - nom];
}

/**
 * @brief Метод вывода стека на екран
 * 
 * @tparam T Тип елементов стека 
 */
template <typename T>
inline void Stack<T>::printStack()
{
    for (int ix = top - 1; ix >= 0; ix--)
        std::cout << "|" << std::setw(4) << stackPtr[ix] << std::endl;
}

/**
 * @brief Метод вернет размер стека.
 * 
 * @tparam T Тип елементов стека
 * @return int Длина стека
 */
template <typename T>
inline int Stack<T>::getStackSize() const
{
    return size;
}

/**
 * @brief Метод вернет указатель на стек (для конструктора копирования)
 * 
 * @tparam T Тип елементов стека
 * @return T* Укащатель на стека
 */
template <typename T>
inline T *Stack<T>::getPtr() const
{
    return stackPtr;
}

/**
 * @brief Метод вернёт размер стека.
 * 
 * @tparam T Тип елементов стека
 * @return int Размер стека
 */
template <typename T>
inline int Stack<T>::getTop() const
{
    return top;
}

#endif // STACK_HPP