#include "Queue.hpp"

template <class C>
Queue<C>::Queue()
{
}

template <class C>
Queue<C>::~Queue()
{
    delete cell_beg;
    delete cell_end;

}

template <class C>
void Queue<C>::push(C value)
{
    this->cell_size++;
    if (this->cell_beg == NULL)
    {
        this->cell_beg = this->cell_end = new Cell(value);
        return;
    }

    this->cell_end->next = new Cell(value);
    this->cell_end = this->cell_end->next;
}

template <class C>
size_t Queue<C>::size()
{
    return this->cell_size;
}

template <class C>
C Queue<C>::back()
{
    return this->cell_end->value;
}

template <class C>
C Queue<C>::front()
{
    return this->cell_beg->value;
}

template <class C>
void Queue<C>::pop()
{
    if (this->cell_beg == NULL)
        return;

    Cell *tmp = this->cell_beg;
    this->cell_beg = this->cell_beg->next;
    this->cell_size--;

    delete tmp;
}

template class Queue<float>;